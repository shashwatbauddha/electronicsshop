// <auto-generated />
namespace ElectronicsShop.Database.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class initialized : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(initialized));
        
        string IMigrationMetadata.Id
        {
            get { return "201812180822584_initialized"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
