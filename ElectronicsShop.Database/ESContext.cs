﻿using ElectronicsShop.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElectronicsShop.Database
{
    public class ESContext : DbContext, IDisposable
    {
        public ESContext() : base("ElectronicsShopConnection")
        {
        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
    }
}
