﻿using ElectronicsShop.Database;
using ElectronicsShop.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElectronicsShop.Services
{
    public class CategoriesService
    {
        // Get Category by Category ID ------------------------------------------------------
        public Category GetCategory(int ID)
        {
            using (var context = new ESContext())
            {
                return context.Categories.Find(ID);
            }
        }

        // Get All Category ------------------------------------------------------------------
        public List<Category> GetCategories()
        {
            using (var context = new ESContext())
            {
                return context.Categories.ToList();
            }
        }

        // Save Category ------------------------------------------------------------------
        public void SaveCategory(Category category)
        {
            using (var context = new ESContext())
            {
                context.Categories.Add(category);
                context.SaveChanges();
            }
        }

        // Update Category by Category ID --------------------------------------------------
        public void UpdateCategory(Category category)
        {
            using (var context = new ESContext())
            {
                context.Entry(category).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
        }

        // Delete Category by Category ID --------------------------------------------------
        public void DeleteCategory(int ID)
        {
            using (var context = new ESContext())
            {
                var category = context.Categories.Find(ID);
                context.Categories.Remove(category);
                context.SaveChanges();
            }
        }
    }
}
