﻿using ElectronicsShop.Database;
using ElectronicsShop.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElectronicsShop.Services
{
    public class ProductsService
    {
        // Get Product by Product ID ------------------------------------------------------
        public Product GetProduct(int ID)
        {
            using (var context = new ESContext())
            {
                return context.Products.Find(ID);
            }
        }

        // Get All Products ------------------------------------------------------------------
        public List<Product> GetProducts()
        {
            using (var context = new ESContext())
            {
                return context.Products.ToList();
            }
        }

        // Save Product ------------------------------------------------------------------
        public void SaveProduct(Product product)
        {
            using (var context = new ESContext())
            {
                context.Products.Add(product);
                context.SaveChanges();
            }
        }

        // Update Product by Product ID --------------------------------------------------
        public void UpdateProduct(Product product)
        {
            using (var context = new ESContext())
            {
                context.Entry(product).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
        }

        // Delete Product by Product ID --------------------------------------------------
        public void DeleteProduct(int ID)
        {
            using (var context = new ESContext())
            {
                var product = context.Products.Find(ID);
                context.Products.Remove(product);
                context.SaveChanges();
            }
        }
    }
}
