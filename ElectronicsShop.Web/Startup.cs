﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ElectronicsShop.Web.Startup))]
namespace ElectronicsShop.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
